package com.education.cloud.course.service.feign.biz;

import com.education.cloud.course.feign.qo.CourseChapterPeriodAuditQO;
import com.education.cloud.course.service.dao.CourseChapterPeriodAuditDao;
import com.education.cloud.course.feign.vo.CourseChapterPeriodAuditVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.education.cloud.course.service.dao.impl.mapper.entity.CourseChapterPeriodAudit;
import com.education.cloud.course.service.dao.impl.mapper.entity.CourseChapterPeriodAuditExample;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.base.PageUtil;
import com.education.cloud.util.tools.BeanUtil;

/**
 * 课时信息-审核
 *
 * @author wujing
 */
@Component
public class FeignCourseChapterPeriodAuditBiz {

    @Autowired
    private CourseChapterPeriodAuditDao dao;

    public Page<CourseChapterPeriodAuditVO> listForPage(CourseChapterPeriodAuditQO qo) {
        CourseChapterPeriodAuditExample example = new CourseChapterPeriodAuditExample();
        example.setOrderByClause(" id desc ");
        Page<CourseChapterPeriodAudit> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, CourseChapterPeriodAuditVO.class);
    }

    public int save(CourseChapterPeriodAuditQO qo) {
        CourseChapterPeriodAudit record = BeanUtil.copyProperties(qo, CourseChapterPeriodAudit.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public CourseChapterPeriodAuditVO getById(Long id) {
        CourseChapterPeriodAudit record = dao.getById(id);
        return BeanUtil.copyProperties(record, CourseChapterPeriodAuditVO.class);
    }

    public int updateById(CourseChapterPeriodAuditQO qo) {
        CourseChapterPeriodAudit record = BeanUtil.copyProperties(qo, CourseChapterPeriodAudit.class);
        return dao.updateById(record);
    }

    public CourseChapterPeriodAuditVO getByVideoNo(Long videoNo) {
        CourseChapterPeriodAudit record = dao.getByVideoNo(videoNo);
        return BeanUtil.copyProperties(record, CourseChapterPeriodAuditVO.class);
    }
}
