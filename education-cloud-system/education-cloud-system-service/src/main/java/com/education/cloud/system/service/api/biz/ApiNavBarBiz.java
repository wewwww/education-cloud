package com.education.cloud.system.service.api.biz;

import java.util.List;

import com.education.cloud.system.common.dto.NavBarDTO;
import com.education.cloud.system.common.dto.NavBarListDTO;
import com.education.cloud.system.service.dao.NavBarDao;
import com.education.cloud.util.base.Result;
import com.education.cloud.util.enums.StatusIdEnum;
import com.education.cloud.util.tools.ArrayListUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.education.cloud.system.common.dto.NavBarDTO;
import com.education.cloud.system.common.dto.NavBarListDTO;
import com.education.cloud.system.service.dao.NavBarDao;
import com.education.cloud.system.service.dao.impl.mapper.entity.NavBar;

/**
 * 头部导航
 *
 * @author wuyun
 */
@Component
public class ApiNavBarBiz {

	@Autowired
	private NavBarDao navBarDao;

	public Result<NavBarListDTO> list() {
		List<NavBar> list = navBarDao.getByStatusId(StatusIdEnum.YES.getCode());
		NavBarListDTO dto = new NavBarListDTO();
		dto.setList(ArrayListUtil.copy(list, NavBarDTO.class));
		return Result.success(dto);
	}

}
