#!/bin/sh

# 配置文件根目录，固定是edu
DOCKERHOME=/edu

# 镜像名称前缀、标签
export BASE_IMAGE_NAME=192.168.153.76/edu-cloud
BSEE_IMAGE_TAG=latest


# 各服务的镜像名称
ADMIN_SERVICE=$BASE_IMAGE_NAME/education-cloud-server-admin:$BSEE_IMAGE_TAG
GATEWAY_SERVICE=$BASE_IMAGE_NAME/education-cloud-gateway:$BSEE_IMAGE_TAG
COURSE_SERVICE=$BASE_IMAGE_NAME/education-cloud-course-service:$BSEE_IMAGE_TAG
SYSTEM_SERVICE=$BASE_IMAGE_NAME/education-cloud-system-service:$BSEE_IMAGE_TAG
USER_SERVICE=$BASE_IMAGE_NAME/education-cloud-user-service:$BSEE_IMAGE_TAG
JOB_SERVICE=$BASE_IMAGE_NAME/education-cloud-job:$BSEE_IMAGE_TAG
UI_SERVICE=$BASE_IMAGE_NAME/education-cloud-ui-web:$BSEE_IMAGE_TAG
WEB_SERVICE=$BASE_IMAGE_NAME/education-cloud-admin-web:$BSEE_IMAGE_TAG


case "$1" in

    # 删除容器
    removeAll)
        echo "* 正在删除容器..."
        time docker rm $(docker ps -aq) -f
        echo "* 删除容器成功..."
        ;;
    # 拉取镜像
    pull)
        echo "* 正在拉取后端镜像..."
        time docker pull $COURSE_SERVICE
        time docker pull $SYSTEM_SERVICE
        time docker pull $USER_SERVICE
        time docker pull $JOB_SERVICE
        time docker pull $ADMIN_SERVICE
        time docker pull $GATEWAY_SERVICE
        echo "* 开始拉取前端镜像..."
        time docker pull $UI_SERVICE
        time docker pull $WEB_SERVICE
        echo "* 拉取镜像成功..."
        ;;
    # 运行镜像
    run)
        echo "* 开始运行基础镜像..."
        time docker-compose -f $DOCKERHOME/docker-compose-base.yml up -d
        echo "* 等待10s..."
        sleep 10
        echo "* 开始运行后端服务镜像..."
        time docker-compose -f $DOCKERHOME/docker-compose-services.yml up -d
        echo "* 等待10s..."
        sleep 10
        echo "* 开始运行前端服务镜像..."
        time docker-compose -f $DOCKERHOME/docker-compose-nginx.yml up -d
        echo "* 运行成功..."
        ;;
    # 拉取镜像并运行
    pullrun)
        echo "* 正在拉取后端镜像..."
        time docker pull $COURSE_SERVICE
        time docker pull $SYSTEM_SERVICE
        time docker pull $USER_SERVICE
        time docker pull $JOB_SERVICE
        time docker pull $ADMIN_SERVICE
        time docker pull $GATEWAY_SERVICE
        echo "* 开始拉取前端镜像..."
        time docker pull $UI_SERVICE
        time docker pull $WEB_SERVICE
        echo "* 拉取镜像成功..."

         echo "* 开始运行基础镜像..."
        time docker-compose -f $DOCKERHOME/docker-compose-base.yml up -d
        echo "* 等待10s..."
        sleep 10
        echo "* 开始运行后端服务镜像..."
        time docker-compose -f $DOCKERHOME/docker-compose-services.yml up -d
        echo "* 等待10s..."
        sleep 10
        echo "* 开始运行前端服务镜像..."
        time docker-compose -f $DOCKERHOME/docker-compose-nginx.yml up -d
        echo "* 运行成功..."
        ;;
    # 停止容器
    stop)
        echo "* 正在停止容器..."
        time docker-compose -f $DOCKERHOME/docker-compose-nginx.yml stop
        time docker-compose -f $DOCKERHOME/docker-compose-services.yml stop
        time docker-compose -f $DOCKERHOME/docker-compose-base.yml stop
        echo "* 停止容器成功..."
        ;;
    # 重启容器
    restart)
        echo "* 正在停止镜像..."
        time docker-compose -f $DOCKERHOME/docker-compose-nginx.yml restart
        time docker-compose -f $DOCKERHOME/docker-compose-services.yml restart
        time docker-compose -f $DOCKERHOME/docker-compose-base.yml restart
        ;;
    # 其它
    *)
        echo "* ..."
        ;;
esac
exit 0
